/*
    This function simply initializes wiringPi, which can only be done
once per program run
    Compile with: gcc -shared -Wl,-soname, init_ADC -o init_ADC.so -fPIC init_ADC -lwiringPi
*/

#include <wiringPi.h>

int main() {
    // Setup using wiringPi Pin Scheme
    if (wiringPiSetup() == -1)
        return -1;

    // BCM GPIO 23 = Board Pin 16 = Wiring Pi Pin 4 = PM Sensor Input
    pinMode(4, OUTPUT);
}
