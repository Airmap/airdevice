# Python script for relatively painless cell module communication.
# Relatively.

from cmd import Cmd
import serial
import sys
import time

# Cell module (cm) serial object
cm = serial.Serial("/dev/ttyAMA0", 9600, timeout=2, parity='N', bytesize=8, stopbits=1)

# Send a serial command to the cell module
def cmCmd(string):
	# Allow calling this function without the AT+
	if string[0:2] != "AT":
		string = "AT+" + string
	# Make sure the string ends with a carriage return (\r) char
	# (Serial.write automatically adds the newline)
	if string[-1] != '\r':
		string = string + '\r'
    # Uncomment below to view sent command in console
	#print("Sending: " + string)
	cm.write(string)
	time.sleep(0.2)

	# Read the string, remove extra whitespace
	rec = cm.read(cm.inWaiting())
	rec = rec[:-1].replace('\r','')

	# Remove the first line, which is just the command
	rec = rec[rec.find('\n')+1:]
	return rec

def cmDataCmd(string):
    # Send simply a raw string
	cm.write(string)
	time.sleep(0.2)

	# Read the string, remove extra whitespace
	rec = cm.read(cm.inWaiting())
	rec = rec.replace('\r','')

	# Remove the first line, which is just the command
	rec = rec[rec.find('\n')+1:]
	return rec

# Send a text message
def cmTextCmd(number, message):
	# Enter SMS Mode
	print(cmCmd("AT+CMGF=1"))
	# Set modem parameters
	print(cmCmd("AT+CSMP=17,167,0,16"))
	# Send the number
	if number[0] != '+':
		number = '+' + number
	print(cmCmd('AT+CMGS="' + number + '"'))
	# Send the text;
	print("Sending message: " + message)
	cm.write(message + '\x1a\r')
	time.sleep(4)
	print(cm.read(cm.inWaiting()).replace('\r',''))

# Custom cmd module wrapper, basically creates a small shell inside
# this python program and passes any commands to the cell module
class CellPassthrough(Cmd):
	prompt = "> "
	intro = "Cell Passthrough Mode Enabled, enter commands:"

	# For unrecognized commands just send the input to the cell module
	def default(self, line):
		print(cmCmd(line))

	def emptyline(self):
		pass

	# Typing 'quit' exits the program
	def do_quit(self, args):
		'''Exits Passthrough Mode'''
		print("Exiting Cell Passthrough Mode")
		sys.exit()

	# Send a text message
	def do_text(self, line):
		'''Send a text message in format 15551231234 Enter your message here!'''
		number = line[0:line.find(' ')]
		message = line[line.find(' ') + 1:]
		cmTextCmd(number, message)


# main function
if __name__ == "__main__":
	# If at.py is called with no argument or 'pass'
	# enter Cell Passthrough Mode
	if len(sys.argv) == 1 or sys.argv[1].lower() == "pass":
		passthrough = CellPassthrough()
		passthrough.cmdloop()
	else:	# Otherwise assume the argument is a cell command
		print(cmCmd(sys.argv[1]))

