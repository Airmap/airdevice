'''
This code provides methods for the SIM808's GSM capability
'''

import logging
import sys
import time
import at
import re
import socket

class GSM(object):
    def __init__(self, ip = '52.72.8.21/post_device_data/', content_type = 'application/json'):
        # Initialize logger
        self.logger = logging.getLogger('airmap.GSM')
        self.logger.info('Initializing GSM connection')

        # Setup the bearer interface (network provider)
        try:
            # Setup the connection type to General Packet Radio Service (GPRS)
            response = at.cmCmd('SAPBR=3,1,"CONTYPE","GPRS"')
            self.logger.debug('Connection to GPRS is {0}'.format(response))
            if response != 'OK':
                self.logger.error('Invalid CONTYPE response', exc_info=True)
            # Set the Access Point Name (APN) to "wholesale" for Ting SIM card
            response = at.cmCmd('SAPBR=3,1,"APN","wholesale"')
            self.logger.debug('Connection to APN is {0}'.format(response))
            if response != 'OK':
                self.logger.error('Invalid APN response', exc_info=True)
            # Set User Profile
            try:
                response = at.cmCmd('SAPBR=1,1')
                if response == 'ERROR':
                    self.logger.debug('User profile already set')
                elif response == 'AT+SAPBR=1,1':
                    self.logger.debug('User profile set')
                else:
                    self.logger.debug('Unexpected response "{0}" during user profile selection'.format(response))
            except Exception, err:
                self.logger.error('Unexpected exception in user profile selection', exc_info=True)
            time.sleep(2)
            # Unknown
            #self.logger.debug(at.cmCmd('CGATT=1'))
            #time.sleep(2)
            # Get IP
            IP = at.cmCmd('SAPBR=2,1')
            # Parse the IP address from the string
            try:
                parsed_IP = re.findall(r'[0-9]+(?:\.[0-9]+){3}', IP)
                # Validate IP
                socket.inet_aton(parsed_IP[0])
                self.logger.debug('Device IP is {0}'.format(parsed_IP[0]))
            except socket.error:
                self.logger.error('Unable to confirm valid IP address', exc_info=True)
            time.sleep(2)
            # Initialize the HTTP connection
            try:
                response = at.cmCmd('HTTPINIT')
                if response == 'ERROR':
                    self.logger.debug('HTTP connection already initialized')
                elif response == 'OK':
                    self.logger.debug('HTTP connection initialized')
                else:
                    self.logger.debug('Unexpected response "{0}" during HTTP initialization'.format(response))
            except Exception, err:
                self.logger.error('Unexpected exception in HTTP initialization', exc_info=True)
            time.sleep(2)
            # Select User
            self.logger.debug('User selection response {0}'.format(at.cmCmd('HTTPPARA="CID",1')))
            # Create URL
            url = 'HTTPPARA="URL","52.72.8.21/post_device_data/"'
            self.logger.debug('URL definition response {0}'.format(at.cmCmd(url)))
            # Set Content type to JSON
            self.logger.debug('Content definition response {0}'.format(at.cmCmd('HTTPPARA="CONTENT","' + content_type + '"')))
        except Exception as e:
            self.logger.error('Unhandled exception in GSM initialization', exc_info=True)

    def send_http_request(self, message):
        # This method sends an HTTP request to the server
        if message is None:
            self.logger.error('No message received to send as HTTP request')
        # Determine the length in bytes of the message
        data_length = len(message)
        self.logger.debug('Sending {0} bytes to the web server'.format(data_length))
        # Set the timeout to 10 seconds (10,000 ms)
        timeout = 10000
        # Create the AT command to specify data length and timeout
        request = 'HTTPDATA=' + str(data_length) + ',' + str(timeout)
        # Send the command
        response = at.cmCmd(request)
        if response != 'DOWNLOAD':
            self.logger.error('No DOWNLOAD response from HTTP interface, unable to send data via HTTP')
            # Break out of the GSM code as we're unable to send data successfully
            return False
        # Send the given message without AT prefix from at.py interface
        time.sleep(1)
        at.cmDataCmd(message)
        time.sleep(1)
        # Send the POST request
        try:
            response = at.cmCmd('HTTPACTION=1')
            # Truncate to simply OK
            #@TODO: This assumes correct response, consider revising
            response = response[0:2]
            self.logger.debug('Response from POST request is {0}'.format(response))
        except Exception, err:
            self.logger.error('Failed to send a HTTP POST request', exc_info=True)
        # Wait for the server to respond
        time.sleep(5)
        # Receive the response from the server
        server_response = at.cmCmd('HTTPREAD')
        # Parse the HTTP status code
        status_code = server_response[15:18]
        return status_code

    def endSession(self):
        at.cmCmd('HTTPTERM')
        at.cmCmd('CGATT=0')

def main():
    gsm = GSM()
    while True:
        gsm.send_http_request('This is a test')
        time.sleep(5)
    gsm.endSession()

if __name__ == '__main__':
    main()
