/*
	Some code taken from ADS1115_sample.c - 12/9/2013. Written by David Purdie as part of the openlabtools initiative
	Initiates and returns a single PM sensor sample through the ADS1115 (without error handling)
    Compile with: gcc -shared -Wl,-soname,ADC -o ADC.so -fPIC ADC.c -lwiringPi -lpthread
*/

#include "ADC.h"
#include <math.h>
#include <pthread.h>
#include <wiringPi.h>
#include <stdio.h>
#include <fcntl.h>     // open
#include <inttypes.h>  // uint8_t, etc
#include <linux/i2c-dev.h> // I2C bus definitions

struct data {
    uint8_t writeBuf[3];    // Buffer to store the 3 bytes that we write to the I2C device
    uint8_t readBuf[2];		// 2 byte buffer to store the data read from the I2C device
    int I2CFile;
};

void *create_PWM(void *param)
{
    digitalWrite(4,1);
    delayMicroseconds(320);
    digitalWrite(4,0);
}

int main() {

    pthread_t thread0;

    // Set highest priority
    piHiPri(99);

    // Declare variables
    int i = 0;
    int k = 0;

    /*
    // Setup using wiringPi Pin Scheme
    if (wiringPiSetup() == -1)
        return 1;

    // BCM GPIO 23 = Board Pin 16 = Pin 4 (wiringPi) = PM Sensor Input
    pinMode(4, OUTPUT);
    */

    // Declare struct object
    struct data dataObj;

    // Initialize ADC
    dataObj = init_ADC(dataObj);

    // Start ADC Conversion (takes ~600 us)
    dataObj = start_conversion(dataObj);

    pthread_create(&thread0, NULL, create_PWM, NULL);

    // End Conversion
    dataObj = end_conversion(dataObj);

    pthread_join(thread0, NULL);

    // Delay to see difference in ADC steps
    delayMicroseconds(500);

    // Close ADC
    close_ADC(dataObj);
}

struct data init_ADC(struct data dataObj) {
    int ADS_address = 0x48;	// Address of our device on the I2C bus

    dataObj.I2CFile = open("/dev/i2c-1", O_RDWR);		// Open the I2C device

    ioctl(dataObj.I2CFile, I2C_SLAVE, ADS_address);   // Specify the address of the I2C Slave to communicate with

  // These three bytes are written to the ADS1115 to set the config register and start a conversion
    dataObj.writeBuf[0] = 1;			// This sets the pointer register so that the following two bytes write to the config register
    dataObj.writeBuf[1] = 0xD3;   	    // This sets the 8 MSBs of the config register (bits 15-8) to 11010011
    dataObj.writeBuf[2] = 0xE3;  		// This sets the 8 LSBs of the config register (bits 7-0) to 11100011

    // Initialize the buffer used to read data from the ADS1115 to 0
    dataObj.readBuf[0]= 0;
    dataObj.readBuf[1]= 0;

    return dataObj;

}

struct data start_conversion(struct data dataObj) {
    // Write writeBuf to the ADS1115, the 3 specifies the number of bytes we are writing,
    // this begins a single conversion
    write(dataObj.I2CFile, dataObj.writeBuf, 3);

    return dataObj;

}

struct data end_conversion(struct data dataObj) {
  // Wait for the conversion to complete, this requires bit 15 to change from 0->1
  while ((dataObj.readBuf[0] & 0x80) == 0)	// readBuf[0] contains 8 MSBs of config register, AND with 10000000 to select bit 15
  {
        read(dataObj.I2CFile, dataObj.readBuf, 2);	// Read the config register into readBuf
  }

  return dataObj;

}

float close_ADC(struct data dataObj) {
    int16_t val;  // Stores the 16 bit value of our ADC conversion
    float raw_value, scaled_value, density;

    dataObj.writeBuf[0] = 0; // Set pointer register to 0 to read from the conversion register
    write(dataObj.I2CFile, dataObj.writeBuf, 1);

    read(dataObj.I2CFile, dataObj.readBuf, 2);		// Read the contents of the conversion register into readBuf

    val = dataObj.readBuf[0] << 8 | dataObj.readBuf[1];	// Combine the two bytes of readBuf into a single 16 bit result

    // Convert from binary value to V
    raw_value = (float)val*4.096/32767.0;

    // Scale due to slow ADC
    // Curve created in MS Excel following data measurements with scope
    scaled_value = -1.4926*pow(raw_value, 2.0) + 5.3794*raw_value - 0.0141;

    // Convert to dust density (ug/m^3)
    // Curve created in MS Excel following manufacturer specifications
    density = 0.04*pow(scaled_value, 2.0) + 0.0348*scaled_value - 0.0092;
    // Convert from mg to ug
    density = density * 100;

    close(dataObj.I2CFile);

    return density;
}
