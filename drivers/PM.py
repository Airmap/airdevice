# This code imports and provides methods for the SIM808

import time
import sys
sys.path.insert(0, '/home/pi/airdevice/dist/Adafruit_Python_GPIO')
sys.path.insert(0, '/home/pi/airdevice/dist/Adafruit_Python_ADS1x15')
import Adafruit_GPIO as GPIO
import Adafruit_ADS1x15 as ADS
from RPIO import PWM

class Fan(object):
    def __init__(self):
        # Call with -O flag to turn off debug
        if __debug__:
            print('Initializing Fan for PM Sensor')
        # Initialize GPIO
        self.GPIO = GPIO.get_platform_gpio()
        # Set Pin 4 as an Output (Fan)
        self.GPIO.setup(4, GPIO.OUT)

    def turnOnFan(self):
    # Turn on the fan
        self.GPIO.output(4, 1)

    def turnOffFan(self):
    # Turn off the fan
        self.GPIO.output(4,0)

class ADS1115(object):
    def __init__(self):
        # Call with -0 flag to turn off debug
        if __debug__:
            print('Initializing ADC for PM Sensor')
        self.ADS = ADS.ADS1115()

    def startADC(self):
        # Define the gain, see table 3 for more info.
        # PGA Setting of 1 provides +/- 4.096V
        # PGA Setting of 2/3 provides +/0 6.xxx V
        self.gain = 2/3
        # Specify the channel to sample
        self.channel = 1
        # Start sampling
        self.ADS.start_adc(self.channel, self.gain)

    def getVoltage(self):
        # Get the raw, 16-bit signed integer value from the ADC
        raw_value = self.ADS.get_last_result()
        if self.gain == 1:
            # Maximum value is (2^15)-1 (Signed 16-bit integer)
            max_value = float(32767)
            # Maximum voltage from datasheet
            max_voltage = 4.096
        elif self.gain == 2/3:
            max_value = float(32767)
            max_voltage = 6.144
        else:
            raise ValueError('Invalid Gain Value for ADS1115')
        vPerBit = max_voltage / max_value
        print('Raw Value {0}'.format(raw_value))
        voltage = raw_value * vPerBit
        return voltage

    def stopADC(self):
        self.ADS.stop_adc()

class PM(object):
    def __init__(self):
        if __debug__:
            print('Initializing PM Sensor PWM')
        # Set DMA to 0, Pulse Width to 10ms
        self.servo = PWM.Servo(0, 10000)

    def generatePWM(self):
        "This function generates a 100Hz PWM Signal for the PM Sensor"
        # Generate PWM signal on GPIO 23 (Pin 16) with .32ms high
        self.servo.set_servo(23, 320)
        # Start time
        self.start_time = time.time()
        print('Start Time {0:.6f}'.format(self.start_time))

def main():
    # Initialize objects
    fan = Fan()
    adc = ADS1115()
    pwm = PM()
    # Turn on the fan
    fan.turnOnFan()
    # Get Data
    print('Sampling Data')
    adc.startADC()
    pwm.generatePWM()
    breakout = True
    while breakout is True:
        time_difference = time.time() - pwm.start_time
        print('Time Difference = {0:.6f}'.format(time_difference))
        if (time_difference > .28e-3):
            print('Voltage {0}'.format(adc.getVoltage()))
            breakout = False
    adc.stopADC()
    # Turn off the fan
    fan.turnOffFan()

if __name__ == '__main__':
    main()
