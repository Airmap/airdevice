'''
This code provides methods for the SIM808 GPS functionality
'''

import pytz
import logging
import time
import datetime as datetime
# Import at.py for communications with SIM808
import at

class SIM808(object):
    def __init__(self):
        # Instantiate a logging object
        self.logger = logging.getLogger('airmap.SIM808')
        # Initialize
        self.logger.info('Initializing SIM808')
        # Check for SIM808 GPS Power
        try:
            raw_power = self.sendCommand('CGNSPWR?')
            power = int(self.stripCommand(raw_power))
            if power == 0:
                self.logger.debug('Turning on SIM808 GPS Power')
                self.sendCommand('CGNSPWR=1')
                self.logger.debug('Waiting for GPS Power')
                # Wait until the power is 1
                while power != 1:
                    power = int(self.stripCommand(self.sendCommand('CGNSPWR?')))
                    time.sleep(1)
            self.logger.debug('GPS Power Activated')
        except Exception, err:
            self.logger.error('Unable to activate GPS power', exc_info=True)

    def sendCommand(self, command):
        # This function uses at.py to send a command and returns the response
        raw_string = at.cmCmd(command)
        return raw_string

    def stripCommand(self, command):
        # This function removes extra lines after the command response
        # and strips the command itself
        # ex. Return command of 'AT+CGNSPWR?' is 0\n OK
        # ex. Function turns that into 0
        #print('stripCommand rawCommand:{0}'.format(command))
        #print(command[10:].splitlines()[0])
        strippedCommand = command[10:].splitlines()[0]
        return strippedCommand

    def getGPS(self):
        # This function returns the NMEA sentance
        try:
            self.NMEA = self.stripCommand(self.sendCommand('CGNSINF'))
            return True
        except Exception, err:
            self.logger.error('Unable to retrieve NMEA sentance', exc_info=True)
            return False

    def parseGPS(self):
        try:
            # This function parses the NMEA sentance
            self.sentance = self.NMEA.split(',')
            self.logger.debug('The raw NMEA sentance is: {0}'.format(self.sentance))
            self.run_status = int(self.sentance[0])
            self.fix_status = int(self.sentance[1])
            # UTC defined here as yyyyMMddhhmmss.sss
            self.UTC = self.sentance[2]
            # Convert to UNIX time
            year = int(self.UTC[0:4])
            month = int(self.UTC[4:6])
            day = int(self.UTC[6:8])
            hour = int(self.UTC[8:10])
            minute = int(self.UTC[10:12])
            second = int(self.UTC[12:14])
            # Create a datetime object
            self.UTC_datetime = datetime.datetime(year, month, day, hour, minute, second)
            # Convert that object to UNIX time
            self.unix_time = float((self.UTC_datetime - datetime.datetime(1970,1,1)).total_seconds())
            # Convert to EST time
            local_tz = pytz.timezone('US/Eastern')
            self.UTC_datetime = self.UTC_datetime.replace(tzinfo=pytz.utc).astimezone(local_tz)
            # Check for valid values
            if not self.fix_status:
                self.logger.error('No GPS fix, invalid time and location data')
                self.lat = 0.0
                self.lng = 0.0
                self.sat_in_view = 0.0
                self.sat_used = 0.0
                self.altitude = 0.0
                return False
            else:
                self.lat = self.sentance[3]
                self.lng = self.sentance[4]
                self.sat_used = self.sentance[14]
                self.sat_in_view = self.sentance[15]
                self.altitude = self.sentance[5]
                return True
        except Exception, err:
            self.logger.error('Unable to parse NMEA sentance',exc_info=True)

    def printData(self):
        try:
            self.logger.debug("SIM808 GPS Data:\nRun Status: {0}\nFix Status: {1}\nUnix Time: {2}\nLatitude: {3}\nLongitude: {4}\nSatelites Used/In View: {5}/{6}\nAltitude (MSL):{7}".format(self.run_status, self.fix_status, self.unix_time, self.lat, self.lng, self.sat_in_view, self.sat_used, self.altitude))
        except Exception, err:
            self.logger.error('Unable to print parsed GPS data, did you try to parseGPS() before you called getGPS()?', exc_info=True)

def main():
    sim = SIM808()
    sim.getGPS()
    sim.parseGPS()
    sim.printData()

if __name__ == '__main__':
    main()
