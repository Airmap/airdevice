'''
Captures and returns a SPEC Ozone Sensor value
'''

import time
import logging
import Adafruit_ADS1x15 as ADS


class SPEC(object):
        def __init__(self):
            # Instantiate logger, inheriting from airmap logger
            self.logger = logging.getLogger('airmap.SPEC')
            # Initialize sensor
            self.logger.info('Initializing ADC for SPEC sensor')
            try:
                self.ADS = ADS.ADS1115()
            except Exception, err:
                logging.error("Failed to create ADS1115 object",exc_info=True)

        def getData(self):
            # Read the SPEC sensor value from ADC AIN2 using default gain
            try:
                raw_value = self.ADS.read_adc(2, gain=1)
            except Exception, err:
                logging.error("Failed to read AIN2",exc_info=True)
            # Convert to a voltage
            voltage = raw_value * 4.096 / 32767.0
            # Convert to Ozone measurement in ppb
            nAmps = voltage / 5e6
            self.ozone = nAmps * 13.71 * 100

        def printData(self):
            try:
                self.logger.info('SPEC Data:\nOzone Value (ppb): {0:.6f}'.format(self.ozone))
            except Exception, err:
                self.logger.error('Data not available, did you call printData() before getData()?', exc_info=True)

def main():
    # Create a SPEC object
    spec = SPEC()
    # Call methods
    spec.getData()
    spec.printData()

if __name__ == '__main__':
    main()
