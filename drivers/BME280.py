'''
This code imports and provides methods for the BME280
'''

import logging
import sys
import time
sys.path.insert(0, '/home/pi/airdevice/dist/Adafruit_Python_BME280')
from Adafruit_BME280 import *

class BME(object):
    def __init__(self):
        # Instantiate logger, inheriting from airmap logger
        self.logger = logging.getLogger('airmap.BME280')
        # Initialize sensor
        self.logger.info('Initializing BME280')
        try:
			self.sensor = BME280(mode=BME280_OSAMPLE_8)
        except ImportError as err:
			logging.error("Failed BME import: {0}".format(err), exc_info=True)

    def getData(self):
		self.degrees = self.sensor.read_temperature()
		self.pressure = self.sensor.read_pressure()
		self.humidity = self.sensor.read_humidity()
		self.sensor_time = self.sensor.t_fine
		self.cpu_time = int(time.time())

    def printData(self):
        try:
            self.logger.debug("BME280 Data:\nTemperature (C): {0:.2f}\nHumidity (%): {1:.2f}\nPressure (Pa): {2:.2f}".format(self.degrees, self.humidity, self.pressure))
        except Exception, e:
            self.logger.error('Data not available, did you call printData() before getData()?', exc_info = True)

def main():
    # Call appropriate methods
    bme = BME()
    bme.getData()
    bme.printData()

if __name__ == '__main__':
	main()
