#!/usr/bin/python

import RPi.GPIO as GPIO
import sys
import time

class CFAL_OLED(object):

    # Pin definitions
    PIN_CLK =  11 # GPIO 11, pinheader #23
    PIN_MOSI = 10 # GPIO 10, pinheader #19
    PIN_MISO = 9  # GPIO 9,  pinheader #21
    PIN_SS   = 8  # GPIO 8,  pinheader #24

    # Initialization
    def __init__(self):
        self.setup_gpio()
        time.sleep(0.01)
        #self.power_seq() # Not sure this is actually needed
        #time.sleep(0.2)
        self.init()

    # Deinitialization
    def __del__(self):
        GPIO.cleanup()

    # Sets up the GPIOs used by the display
    # Currently it only uses SPI pins
    def setup_gpio(self):
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(self.PIN_CLK,  GPIO.OUT)
        GPIO.setup(self.PIN_MOSI, GPIO.OUT)
        GPIO.setup(self.PIN_MISO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.PIN_SS,   GPIO.OUT)

        # CS is active low, CPOL=1 (idle state is HIGH)
        GPIO.output(self.PIN_SS, True)
        GPIO.output(self.PIN_CLK, True)

    # Bit-banging SPI implementation
    # b: number of bits to send
    # cmd: value to send
    def send_bits(self, b, cmd):
        # Set SS LOW
        GPIO.output(self.PIN_SS, False)

        # Loop for sending the individual bits
        # Masks each bit (starting at the MSB) individually
        # and checks if the result has a '1' in it or not
        for i in range(1, b+1):
            bit = cmd & (0b1 << (b-i))
            GPIO.output(self.PIN_MOSI, (bit != 0))
            GPIO.output(self.PIN_CLK, False)
            GPIO.output(self.PIN_CLK, True)

        #Set SS HIGH again
        GPIO.output(self.PIN_MOSI, True)
        GPIO.output(self.PIN_SS, True)

    # Wait for the WS0010 to clear the busy flag
    # Bit-banging implementation
    def wait_for_idle(self, timeout=5):
        busy = True;
        cmd = "0100000000"
        while (busy and (timeout >= 0)):
            for i in range(0, 10):
                GPIO.output(self.PIN_MOSI, (cmd[i]=='1'))
                GPIO.output(self.PIN_CLK, False)
                if (i == 2):
                    busy = GPIO.input(self.PIN_MISO)
                GPIO.output(self.PIN_CLK, True)
            timeout = timeout - 1;
            #print("Busy flag: " + str(busy))

    # Send a command (RS=0, R/WB=0) to the WS0010
    def command(self, cmd):
        self.send_bits(10, cmd & 0xff)
        self.wait_for_idle()
        #time.sleep(0.02)

    # Send data to the WS0010 DDRAM (RS=1, R/WB=0)
    def data(self, data):
        self.send_bits(10, 0x200 | (data & 0xff))

    # Power-on sequence simulation from the CrystalFontz demo code
    def power_seq(self):
        self.clear()
        self.command(0x30) # Function Set
        self.command(0x13) # Internal power off
        self.command(0x08) # Display on/off: Off, no cursor
        self.command(0x04) # Entry mode: decrement, no shift
        self.command(0x14) # Cursor mode: Auto shift cursor position to right
        self.command(0x17) # Power on

    # Initialization sequence, based on:
    # https://www.raspberrypi.org/forums/viewtopic.php?t=68055&p=498048
    # Also see page 19 of the WS0010 datasheet
    def init(self):
        time.sleep(0.1)
        self.command(0x3b) # Function Set: 8-bit, 2-line, 5x8, Western II
        self.command(0x08) # Display on/off: Off, no cursor
        self.clear()
        self.command(0x06) # Entry mode: increment, no shift
        self.command(0x17) # Internal power on, char mode
        self.command(0x0c) # DIsplay ON/OFF: on, no cursor/blink
        self.home()

    # Clear the display
    # Erases anything stored in DDRAM
    def clear(self):
        self.command(0x01) # Clear Display Instruction

    # Set the cursor to the home position
    def home(self):
        self.command(0x02) # Return Home Instruction

    # Set the cursor position
    def set_cursor(self, line, col):
        if line == 0:
            self.command(0x80 | (col & 0x3f))
        elif line == 1:
            self.command(0x80 | 0x40 | (col & 0x3f))
        else:
            print("set_cursor invalid line: " + str(line))

    # Set an entire line on the display
    def set_line(self, line_number, string):
        # Check string length and truncate
        if len(string) > 16:
            string = string[:14] + (string[14:] and '..')
        # Ensure 16 characters
        string.ljust(16)
        # Set cursor
        self.set_cursor(line_number, 0)
        # Write the string
        self.set_string(string)

    # Set one or more characters at the current cursor position
    # Note: The character set we're using (Western II) matches the standard ASCII table
    # (http://www.asciitable.com/) for pretty much every standard character
    def set_string(self, string):
        # Clear the current string first
        # self.clear_display()
        # Send characters
        for char in string:
            self.data(ord(char))


if __name__ == "__main__":
    line1 = "-.-.-Line 1-.-.-"
    line2 = ".-.-.Line 2.-.-."
    if (len(sys.argv) > 1):
        line1 = sys.argv[1]
    if (len(sys.argv) > 2):
        line2 = sys.argv[2]

    oled = CFAL_OLED()
    oled.set_cursor(0, 0)
    oled.set_string(line1)
    oled.home()
    oled.set_cursor(1, 0)
    oled.set_string(line2)
