#!/usr/bin/python
# -*- coding: latin-1 -*-

'''
This script samples the various sensors, writes to the database and
creates an HTTP Post Request to send to the server (if possible)
'''

import pdb
import datetime
import random
import time
import json
import logging
import ctypes
import sys
import Adafruit_GPIO as GPIO
import Adafruit_ADS1x15
from mongoengine import connect
from threading import Thread
from models import Device, DeviceRecord, GPS
from SIM808 import SIM808
from BME280 import BME
from GSM import GSM
from OLED import CFAL_OLED
from SPEC import SPEC

class DAQ(object):
    def __init__(self):
        '''
        This function initializes sensor objects and
        various script parameters
        '''
        # Initialize the timer/counter
        self.start_time = time.time()
        # Define the sampling frequency (Hz) for the device
        self.fs = float(1)/float(10)
        # Define the sampling period
        self.T = 1/self.fs

        # Instantiate a logger object
        self.logger = logging.getLogger('airmap')

        # Instantiate sensor objects
        self.bme = BME()
        self.sim = SIM808()
        self.oled = CFAL_OLED()
        self.adc = Adafruit_ADS1x15.ADS1115()
        self.spec = SPEC()
        self.gsm = GSM('52.72.8.21',"application/json")
        # Initialize wiringPi once for PM C Code
        self.init_ADC = ctypes.CDLL('/home/pi/airdevice/init_ADC.so')
        self.init_ADC.main()
        # PM C Code
        self.PM25 = ctypes.CDLL('/home/pi/airdevice/ADC.so')
        self.PM25.main.restype = ctypes.c_float

        # Setup GPIO as Inputs for Sample and Display switches
        self.GPIO = GPIO.get_platform_gpio()
        # Display = GPIO 13 = Pin 33 - Pull-Up, Active Low
        self.GPIO.setup(13, GPIO.IN, GPIO.PUD_UP)
        # Sampling = GPIO 24 = Pin 18 - Pull-Up, Active Low
        self.GPIO.setup(24, GPIO.IN, GPIO.PUD_UP)

        # Add interrupts for switches
        #self.GPIO.add_event_detect(24, GPIO.FALLING, callback=self.displayInterruptOn)

        # Instantiate a logger object
        self.logger = logging.getLogger('airmap')

    def sampleSensors(self):
        # Get the current time
        self.curr_time = time.time()
        # Get the time difference
        self.time_diff = self.curr_time - self.start_time
        # Wait if the elapsed time is less than the interval
        # between samples
        while (self.time_diff < self.T):
            # Increment the current time
            self.curr_time = time.time()
            # Determine the time difference
            self.time_diff = self.curr_time - self.start_time
        # Time to sample!
        # Increment the start time by the sampling period
        self.start_time += self.T

        # Determine switch positions
        # Create flags, defaulting to True
        DISPLAY = True;
        SAMPLE = True;
        # Display, Active Low
        if self.GPIO.input(13):
            self.logger.info('Display Off Requested')
            DISPLAY = False
        else:
            self.logger.info('Display On Requested')
            DISPLAY = True
        # Sampling, Active Low
        if self.GPIO.input(24):
            self.logger.info('Sampling Off Requested')
            SAMPLE = False
        else:
            self.logger.info('Sampling On Requested')
            SAMPLE = True

        # Call sensor methods if appropriate
        # Retrieve GPS NMEA sentance
        if not SAMPLE:
            # Log and Display
            self.logger.info('Waiting to sample')
            if DISPLAY:
                t = Thread(target=self.printWait,)
                t.setDaemon(True)
                t.start()
            else:
                t = Thread(target=self.printNothing,)
                t.setDaemon(True)
                t.start()
        elif not self.sim.getGPS() or not self.sim.parseGPS():
            # Log an error and display on screen before waiting for next sample time
            self.logger.info('Unable to geolocate, sample purged')
            # Display on screen, creating a daemon thread to non-block
            if DISPLAY:
                t = Thread(target=self.printBadGPS,)
                t.setDaemon(True)
                t.start()
            else:
                t = Thread(target=self.printNothing,)
                t.setDaemon(True)
                t.start()
        else:
            # Sample
            # Log GPS data
            self.sim.printData()

            # Atmospheric Data
            self.bme.getData()
            self.bme.printData()

            # PM2.5 Data
            # Note: Not a class due to implementation in C
            self.PM25.value = self.PM25.main()
            # Ensure that we don't have a negative value
            self.PM25.value = max(self.PM25.value, 0)
            # Log as an integer, ensuring 3 digits
            self.logger.info('PM 2.5 Data\nPM 2.5: {0:03d} ug/m^3'.format(int(self.PM25.value)))

            # Ozone Data
            self.spec.getData()
            self.spec.printData()

            # Populate DB Object
            self.populateDbRecord()

            # Display on screen, creating a daemon thread to non-block
            if DISPLAY:
                t = Thread(target=self.printToOLED,)
                t.setDaemon(True)
                t.start()
            else:
                t = Thread(target=self.printNothing,)
                t.setDaemon(True)
                t.start()

            # Create a dictionary for web server to receive
            json_obj = json.loads((self.record).to_json())
            device_data = {'data':json_obj}
            key = self.api_key
            device_data["device_api_key"]=key

            # Send the HTTP request and get the status code back
            status_code = self.gsm.send_http_request(json.dumps(device_data))
            if not status_code:
                self.logger.warning('Unable to send HTTP request')
            else:
                # Log the status code
                self.logger.info('Received Status Code {0} from the server'.format(status_code))
            # Save after we send so local ID field isn't populated
            try:
                self.record.save()
                self.logger.info('Succesfully wrote to local database')
            except Exception, err:
                self.logger.error('Unable to write to local database', exc_info=True)

    def printToOLED(self):
        '''
        This function creates different panes of display and displays
        them to the OLED display and different intervals
        '''
        ''' Status Screen '''
        # Populate data
        date = self.sim.UTC_datetime.strftime("%H:%M  %m/%d/%y")
        # Retrieve battery level from ADC (AIN0) with default gain of +/- 4.096V
        raw_value = self.adc.read_adc(0, gain=1)
        # Convert the raw reading to V
        raw_battery = raw_value * 4.096 / 32767.0
        # Multiply the ADC value by 2 to adjust for the voltage divider
        battery_voltage = raw_battery * 2.0
        gps_status = self.sim.fix_status
        # Convert so "non-nerds can read"
        gps_status = 'Y' if gps_status else 'N'
        # Clear screen
        self.oled.clear()
        # Create lines
        status_line_1 = 'BATT:{0:.2f}V GPS:{1}'.format(battery_voltage, gps_status)
        # Write to display
        self.oled.set_line(0, date)
        self.oled.set_line(1, status_line_1)
        # Sleep
        time.sleep(3)

        ''' Info Pane I '''
        # Populate data
        pm25 = self.record.pm25_concentration
        ozone = self.record.ozone_concentration
        # Clear screen
        self.oled.clear()
        # Create lines
        # Cast the float PM 2.5 concentration (ug/m^3) to an int and pad to ensure 3 digits for display-niceness
        status_line_1 = 'PM2.5: {0:02d} \xEAg/m\x1f'.format(int(pm25))
        status_line_2 = 'Ozone: {0:.1f} ppb'.format(ozone)
        # Write to display
        self.oled.set_line(0, status_line_1)
        self.oled.set_line(1, status_line_2)
        # Sleep
        time.sleep(3)

        ''' Info Pane II '''
        # Populate data
        temp = self.bme.degrees
        humid = self.bme.humidity
        press = self.bme.pressure
        # Clear screen
        self.oled.clear()
        # Create lines
        status_line_1 = 'Temp:    {0:.2f}\xb2C'.format(temp)
        status_line_2 = 'Humidity: {0:.2f}%'.format(humid)
        # Write to display
        self.oled.set_line(0, status_line_1)
        self.oled.set_line(1, status_line_2)
        # Sleep
        time.sleep(3)

    def printBadGPS(self):
        self.oled.clear()
        self.oled.set_line(0, 'Waiting for GPS')

    def printWait(self):
        self.oled.clear()
        self.oled.set_line(0, 'Waiting to')
        self.oled.set_line(1, 'Sample Data')

    def printNothing(self):
        self.oled.clear()

    def populateDbRecord(self):
        self.logger.debug('Populating database object')
        # Connect to the MongoDB Database
        try:
            connect('airdb')
        except Exception, err:
            self.logger.error('Unable to connect to "airdb" database', exc_info=True)
        # Populate GPS DB Object
        gps = GPS(latitude = self.sim.lat,
            longitude = self.sim.lng,
            number_satellites = self.sim.sat_used,
            run_status = self.sim.run_status,
            fix_status = self.sim.fix_status,
            unix_time = self.sim.unix_time,
            # Uncomment below to test with current time
            #unix_time = float((datetime.datetime.now()-datetime.datetime(1970,1,1)).total_seconds()))
            altitude = self.sim.altitude,
            altitude_units = 'meters MSL')
        # Get Device ID
        id = Device.objects()[0].device_id
        # Get API Key
        self.api_key = Device.objects()[0].api_key
        # Populate remaining fields
        self.record = DeviceRecord(
                device_id=id,
                # Uncomment below to test with current time
                #datetime = datetime.datetime.now(),
                datetime=self.sim.UTC_datetime,
                gps = gps,
                temperature = self.bme.degrees,
                temperature_units = 'C',
                humidity = self.bme.humidity,
                humidity_units = '%',
                pressure = self.bme.pressure,
                pressure_units = 'Pa',
                ozone_concentration = self.spec.ozone,
                ozone_units = 'ppb',
                pm25_concentration = self.PM25.value,
                pm25_units = 'ug/m^3')

def main():
    # Initialize the base logger 'airmap'
    logger = logging.getLogger('airmap')
    # Set the threshold level at which to record events
    logger.setLevel(logging.DEBUG)

    # Create console handle for console ouput
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # Create file handle for output log file
    fh = logging.FileHandler('output.log')
    # Set the threshold level at which to write events to file
    fh.setLevel(logging.INFO)

    # Create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # Add formatter to handles
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # Add handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    # Instantiate Objects
    daq = DAQ()

    while True:
        # Sample the sensors, write to DB, and send HTTP, repeat
        daq.sampleSensors()

if __name__ == '__main__':
    main()
