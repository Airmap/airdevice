import requests
from mongoengine import *
from models import *
import datetime
import json
import time
from GSM import GSM

####
import pdb
###

# TODO: set up device logs

# creates and stores a {@code DeviceRecord}s and returns the string to POST
def post_data(ip):
    key = Device.objects()[0].api_key
    gsm = GSM(ip,"application/json")
    device_data = {'data':get_device_data()}
    device_data["device_api_key"]=key
    gsm.send_http_request(json.dumps(device_data))
    #TODO get status code from send_http_request
    print('successful data update')
    # TODO: log this once we set up device logs

def get_device_data():
    current_time = datetime.datetime.now()
    dev_id = Device.objects()[0].device_id
    # Test.. build obj dict here
    record_dict = {
                    "device_id":dev_id,
                    "datetime":current_time,
                    "gps":{
                            "latitude":35,
                            "longitude":135,
                            "number_satellites":2,
                          },
                    "temperature":20.0,
                    "temperature_units":'C',
                    "humidity":0.40,
                    "humidity_units":'%',
                    "pressure":1.0123,
                    "pressure_units":'atm',
                    "altitude":123,
                    "altitude_units":'m',
                    "ozone_concentration" : 5,
                    "ozone_units" : 'idfk',
                    "pm25_concentration" : 105,
                    "pm25_units" : 'ppm',
                   }
    # and send
    test_obj = json.loads(DeviceRecord(**record_dict).to_json())
    return test_obj

if __name__=="__main__":
    connect('airdb')
    post_data("localhost:80/post_device_data/")
