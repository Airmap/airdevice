from mongoengine import *

# Place model classes here
class GPS(EmbeddedDocument):
    latitude = FloatField()
    longitude = FloatField()
    number_satellites = IntField()
    run_status = IntField()
    fix_status = IntField()
    unix_time = FloatField()
    altitude = FloatField()
    altitude_units = StringField()

class DeviceRecord(Document):
    device_id = IntField()
    # datetime object (e.g. datetime.datetime(2016, 9, 11, 19, 3,30,500000) for 7:03:30.5
    datetime = DateTimeField()
    gps = EmbeddedDocumentField(GPS)

    temperature = FloatField()
    temperature_units = StringField()
    humidity = IntField()
    humidity_units = StringField()
    pressure = FloatField()
    pressure_units = StringField()

    ozone_concentration = FloatField()
    ozone_units = StringField()
    pm25_concentration = FloatField()
    pm25_units = StringField()

class Device(Document):
    api_key = StringField()
    device_id = IntField()

if __name__=="__main__":
    connect('airdb')
    import pdb
    pdb.set_trace()
    pass
